<%inherit file="/base.mako"/>
<%!
    section = 'home'

    # 1
%>

<h1>The Python SQL Toolkit and Object Relational Mapper</h1>

<p>SQLAlchemy is the Python SQL toolkit and Object Relational Mapper that
gives application developers the full power and flexibility of SQL.</p>
<p>It provides a full suite of well known enterprise-level persistence
patterns, designed for efficient and high-performing database access, adapted
into a simple and Pythonic domain language.</p>

<h2>SQLALCHEMY'S PHILOSOPHY</h2>

<p class="philos">SQL databases behave less like object collections the more
size and performance start to matter; object collections behave less like
tables and rows the more abstraction starts to matter. SQLAlchemy aims to
accommodate both of these principles.</p>

<p>SQLAlchemy considers the database to be a relational algebra engine,
    not just a collection of tables.   Rows can be selected from not only
    tables but also joins and other select statements; any of these
    units can be composed into a larger structure.   SQLAlchemy's expression
    language builds on this concept from its core.</p>

<p>SQLAlchemy is most famous for its object-relational mapper (ORM), an optional
    component that provides the <strong>data mapper pattern</strong>, where
    classes can be mapped to the database in open ended, multiple ways -
    allowing the object model and database schema to develop in a cleanly
    decoupled way from the beginning.</p>

<p>The main goal of SQLAlchemy is to change the way you think about databases
and SQL! </p>

<p><strong>Read some <a href="/features.html"><strong>key features</strong></a> of SQLAlchemy,
    as well as <a href="/quotes.html"><strong>what people are saying</strong></a> about SQLAlchemy.</strong>
    </p>

<dl id="features">
    <dt>Who Uses SQLAlchemy</dt>
    <dd>
        <p>SQLAlchemy is used by organizations such as:
        <ul>
            <li><a href="/organizations.html#yelp">Yelp!</a></li>
            <li><a href="/organizations.html#openstack">The OpenStack Project</a></li>
            <li><a href="/organizations.html#reddit">reddit</a></li>
            <li><a href="/organizations.html#firefox">Mozilla</a></li>
            <li><a href="/organizations.html#surveymonkey">Survey Monkey</a></li>
            <li><a href="/organizations.html#lolapps">lolapps</a></li>
            <li><a href="/organizations.html#freshbooks">Freshbooks</a></li>
            <li><a href="/organizations.html#fedoraproject">Fedora Project</a></li>
        </ul>
        </p>
        <a href="/organizations.html">more...</a>
    </dd>
    <dt>Learn More</dt>
    <dd>
        <ul>
            <li><a href="/library.html"><strong>Library</strong></a> - master index of documentation, videos, talks, and more</li>
            <li><a href="/features.html">Overview of Key Features</a></li>
            <li><a href="/organizations.html">Organizations Using SQLAlchemy</a></li>
            <li><a href="/quotes.html">Testimonials</a></li>
        </ul>
    </dd>
    <dt>Resources</dt>
    <dd>
        <ul>
            <li><a href="/download.html">Download</a></li>
            <li><a href="/blog/">Blog</a></li>
            <li><a href="/support.html">Getting Support</a></li>
            <li><a href="/participate.html">Participate</a></li>
            <li><a href="/develop.html">Development</a></li>
        </ul>
    </dd>
</dl>




