<%inherit file="/base.mako"/>
<%!
section='involved'
%>
<%block name="head_title">
Get Involved - SQLAlchemy
</%block>

<h1>Community Guide</h1>

<p>A guide to getting involved with SQLAlchemy.</p>
<ul class="nav">
    <li><a href="/support.html">Get Support</a> | </li>
    <li><a href="/participate.html">Participate</a> | </li>
    <li><a href="/develop.html">Develop</a></li>
</ul>

${next.body()}